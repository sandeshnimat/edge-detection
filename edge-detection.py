from skimage import io
import os
from matplotlib import pyplot as plt
import numpy as np


def load_image(filename):
    """
    loads image at the given file path

    Args:
        filename (str): path to the image file

    Raises:
        OSError: file does not exist

    Returns:
        image (numpy.ndarray): image loaded in grayscale
    """

    # Check if file exists and return the image loaded from the file
    if not os.path.exists(filename):
        raise OSError("Image file does not exist.")
    else:
        return io.imread(filename, as_gray=True)



def display_image(image, title=None):
    """
    displays the given image

    Args:
        image (numpy.ndarray): loaded image array
        title (str): title of the image to be displayed (optional)

    Returns:
        None: displays the image
    """
    # Plot the image in grayscale format
    plt.imshow(image, cmap='gray')
    
    # Set the title if provided
    if title != None:
        plt.title(title)

    # Remove axis markers from the plot
    plt.xticks([])
    plt.yticks([])    

    # Display the image
    plt.show()


def apply_filter(filter, image, offset=0.0):
    """
    applies the the given filter to the given image

    Args:
        filter (numpy.ndarray): numpy array of filter, should have odd shape along both axes
        image (numpy.ndarray): input image over which filter will be applied
        offset (float): offset between 0 to 1, default 0.0

    Raises:
        assert exception if filter shape is not odd

    Returns:
        output_image (numpy.ndarray): output image after applying filter
    """
    assert filter.shape[1] % 2 == 1, "Filter shapes should be odd. Try again with odd shaped filters."
    assert filter.shape[0] % 2 == 1, "Filter shapes should be odd. Try again with odd shaped filters."

    # Determines the center of the filter for approriate padding.
    f_center = [filter.shape[0] // 2, filter.shape[1] // 2]
    
    # Apply padding to given image
    padded_image = np.zeros((image.shape[0] + filter.shape[0] - 1, image.shape[1] + filter.shape[1] - 1),
                            dtype='float')
    padded_image[f_center[0]: -f_center[0], f_center[1]: -f_center[1]] = image

    # Initialize output image with all zeros
    output_image = np.zeros_like(image)

    # Iterate over each pixel in the image.
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            # Apply filter to each pixel
            convolution = filter * padded_image[i: i + filter.shape[0],j: j + filter.shape[1]]
            
            # Replace corresponding pixel in output image with convolution plus offset
            output_image[i, j] = convolution.sum() + offset

    return output_image


def calc_gradient(horz_sobel, vert_sobel):
    """
    compute resultant image by calculating gradient of images resulted from applying
    horizontal and vertical sobel filters

    Args:
        horz_sobel (numpy.ndarray): output image after applying horizontal sobel filter
        vert_sobel (numpy.ndarray): output image after applying vertical sobel filter

    Returns:
        output_image (numpy.ndarray): output image after calculating gradient

    """
    # Calculate gradient using euclidean distance metric
    output_image = (horz_sobel ** 2 + vert_sobel ** 2) ** 0.5

    return output_image


def normalize(image):
    """
    normalizes the given image by subtracting minimum and dividing by maximum

    Args:
        image (numpy.ndarray): input image

    Returns:
        image (numpy.ndarray): output image after normalizing    
    """
    # Subtract the minimum, and then divide by the maximum
    image -= image.min()
    image /=  image.max()
    return image


if __name__ == '__main__':
    image_file = './sample_image.jpg'

    # Load the image at given path
    image = load_image(image_file)

    # Display the image
    display_image(image, "input image")

    # Define horizontal and vertical sobel filters
    sobel_h = np.array(
                    [[1, 2, 1],
                    [0, 0, 0],
                    [-1, -2, -1]],
                    dtype='float'
                )
        

    sobel_v = np.array(
                    [[-1, 0, 1],
                    [-2, 0, 2],
                    [-1, 0, 1]],
                    dtype='float'
                )


    # Apply horizontal and vertical sobel filters and disply results
    horz_sobel = apply_filter(sobel_h, image)
    vert_sobel = apply_filter(sobel_v, image)

    display_image(horz_sobel, "horizontal sobel filter output")
    display_image(vert_sobel, "vertical sobel filter output")

    # Calculate gradient
    output_image = calc_gradient(horz_sobel, vert_sobel)

    # Normalize output image and display final output
    output_image = normalize(output_image)

    display_image(output_image, "edge detection final output")

    # Save output image
    io.imsave("output_image.jpg", output_image)



    

    

    

    