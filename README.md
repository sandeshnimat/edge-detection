### Edge Detection

In this introductory project, we were able to apply simple sobel filter to any given image to detect edges in given image.

After completing this project, we were able to understand:
* How to apply any filter to an image?
* How diftferent filters affect the given image?
* How to calculate gradient of results obtained after applying horizontal and vertical sobel filter?